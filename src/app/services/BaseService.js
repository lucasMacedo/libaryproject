import database from '../../database';

class BaseService {

    constructor(model) {
        this.model = model;
    }

    async getAll() {
        try {
            return await database[this.model].findAll();
        } catch (error) {
            throw error;
        }
    }

    async store(newData) {
        try {
            return await database[this.model].create(newData);
        } catch (error) {
            throw error;
        }
    }

    async update(id, data) {
        try {
            const recordToUpdate = await database[this.model].findOne({
                where: { id: Number(id) }
            });

            if (recordToUpdate) {
                await database[this.model].update(data, { where: { id: Number(id) } });

                return data;
            }

            return null;
        } catch (error) {
            throw error;
        }
    }

    async getById(id) {
        try {
            const record = await database[this.model].findOne({
                where: { id: Number(id) }
            });

            return record;
        } catch (error) {
            throw error;
        }
    }

    async delete(id) {
        try {
            const recordDelete = await database[this.model].findOne({ where: { id: Number(id) } });

            if (recordDelete) {
                const deletedRecord = await database[this.model].destroy({
                    where: { id: Number(id) }
                });
                return deletedRecord;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }
}

export default BaseService;