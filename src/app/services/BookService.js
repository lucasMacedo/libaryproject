import BaseService from "./BaseService";

class BookService extends BaseService {

    constructor() {
        let model = "Book";
        super(model);
    }

}

export default BookService;