require('dotenv/config');

module.exports = {
  development: {
    database: 'libary_db',
    username: 'root',
    password: 'root',
    host: '127.0.0.1',
    dialect: 'mysql'
  },
  production: {
    use_env_variable: "JAWSDB_MARIA_URL",
    dialect: 'mysql'
  }
};