(function (window) {
    window.__env = window.__env || {};
    window.__env.apiUrl = 'https://sheltered-beach-34101.herokuapp.com/api/v1/';
    window.__env.enableDebug = false;
}(this));